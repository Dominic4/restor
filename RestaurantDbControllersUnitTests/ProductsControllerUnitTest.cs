﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestaurantDataAccessLayer.DbControllers;
using RestaurantDataAccessLayer.Entities;

namespace RestaurantDbControllersUnitTests
{
    [TestClass]
    public class ProductsControllerUnitTest
    {
        [TestMethod]
        public void SelectProductsTestMethod()
        {
            List<Product> list = ProductDBController.GetProduct();
            Assert.IsTrue(list != null);
        }

        [TestMethod]
        public void AddProductTestMethod()
        {
            List<Product> list = ProductDBController.GetProduct();
            int count = list.Count;
            bool res = ProductDBController.AddProduct("Potato", "gr");
            Assert.AreEqual(res, true);
            List<Product> resList = ProductDBController.GetProduct();
            Assert.AreEqual(resList.Count, ++count);
            Assert.AreEqual(resList[count-1].Name, "Potato");
            Assert.AreEqual(resList[count-1].Unit, "gr");
        }

        [TestMethod]
        public void DeleteProductTest()
        {
            bool resAdd = ProductDBController.AddProduct("Potato", "gr");
            Assert.IsTrue(resAdd);
            List<Product> list = ProductDBController.GetProduct();
            int count = list.Count;
            bool resDel = ProductDBController.DeleteProduct(list[count-1].Id);
            Assert.IsTrue(resDel);
            Assert.AreEqual(ProductDBController.GetProduct().Count, --count);
        }

    }
}
