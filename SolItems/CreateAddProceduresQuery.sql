USE RestaurantDB
GO
CREATE PROCEDURE [dbo].[AddDishSP]
	-- Add the parameters for the stored procedure here
	@name nvarchar(32),
	@recipeId int,
	@portionSize int,
	@unit nvarchar(10),
	@price float
AS

    -- Insert statements for procedure here
	insert into tbDishes(Name, RecipeId, PortionSize, Unit, Price)
	values (@name, @recipeId, @portionSize, @unit, @price)

	select scope_identity()
GO
CREATE PROCEDURE [dbo].[AddLayoutSP]
	-- Add the parameters for the stored procedure here
	@idDish int,
	@idProduct int,
	@quantity float
AS

    -- Insert statements for procedure here
	insert into tbLayout(IdDish, IdProduct, Quantity)
	values (@idDish, @idProduct, @quantity)

	select scope_identity()
GO
CREATE PROCEDURE [dbo].[AddProductSP]
	-- Add the parameters for the stored procedure here
	@name nvarchar(32),
	@unit nvarchar(10)
AS

    -- Insert statements for procedure here
	insert into tbProducts(Name, Unit)
	values (@name,@unit)

	select scope_identity()
GO
CREATE PROCEDURE [dbo].[AddPurchaseSP]
	-- Add the parameters for the stored procedure here
	@idProduct int,
	@quantity float,
	@price float,
	@unit nvarchar(10),
	@totalAmount float
AS

    -- Insert statements for procedure here
	insert into tbPurchases(IdProduct, Quantity, Price, Unit, TotalAmount)
	values (@idProduct, @quantity, @price, @unit, @totalAmount)

	select scope_identity()
GO
CREATE procedure [dbo].[AddRecipeSP]
	@nameDish nvarchar(32),
	@recipe nvarchar(1000)
as
	insert into tbRecipes(NameDishes, Recipe)
	values(@nameDish, @recipe)
	select scope_identity()
GO
CREATE PROCEDURE [dbo].[AddSaleSP]
	-- Add the parameters for the stored procedure here
	@idDish int,
	@numOfSales int,
	@totalAmount float

AS
    -- Insert statements for procedure here
	insert into tbSales(IdDish, NumOfSales, TotalAmount)
	values (@idDish, @numOfSales, @totalAmount)

	select scope_identity()
GO