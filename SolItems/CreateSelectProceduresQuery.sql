USE RestaurantDB
GO
CREATE PROCEDURE [dbo].[SelectDishesSP]
AS
    SELECT * FROM tbDishes 
GO
CREATE PROCEDURE [dbo].[SelectLayoutsSP]
AS
    SELECT * FROM tbLayout
GO
CREATE PROCEDURE  [dbo].[SelectPriceFromtbDishesSP]
AS
SELECT tbDishes.Price, tbSales.IdDish from tbDishes, tbSales where tbDishes.Id = tbSales.IdDish
GO
CREATE PROCEDURE [dbo].[SelectProductsSP]
AS
    SELECT * FROM tbProducts 
GO
CREATE PROCEDURE [dbo].[SelectPurchasesSP]
AS
    SELECT * FROM tbPurchases 
GO
CREATE PROCEDURE [dbo].[SelectRecipesSP]
AS
    SELECT * FROM tbRecipes 
GO
CREATE PROCEDURE [dbo].[SelectSalesSP]
AS
    SELECT * FROM tbSales 
GO