
USE RestaurantDB
GO
CREATE PROCEDURE [dbo].[DeleteDishSP]
	-- Add the parameters for the stored procedure here
	@id int
AS

    -- Insert statements for procedure here
	delete from tbDishes
	where Id = @id

	select scope_identity()
GO
CREATE PROCEDURE [dbo].[DeleteLayoutSP]
	-- Add the parameters for the stored procedure here
	@id int
AS

    -- Insert statements for procedure here
	delete from tbLayout
	where Id = @id

	select scope_identity()
GO
CREATE PROCEDURE [dbo].[DeleteProductSP]
	-- Add the parameters for the stored procedure here
	@id int
AS

    -- Insert statements for procedure here
	delete from tbProducts
	where Id = @id

	select scope_identity()
GO
CREATE PROCEDURE [dbo].[DeletePurchaseSP]
	-- Add the parameters for the stored procedure here
	@id int
AS

    -- Insert statements for procedure here
	delete from tbPurchases
	where Id = @id

	select scope_identity()
GO
CREATE PROCEDURE [dbo].[DeleteRecipySP]
	-- Add the parameters for the stored procedure here
	@id int
AS

    -- Insert statements for procedure here
	delete from tbRecipes
	where Id = @id

	select scope_identity()
GO
CREATE PROCEDURE [dbo].[DeleteSaleSP]
	-- Add the parameters for the stored procedure here
	@id int
AS

    -- Insert statements for procedure here
	delete from tbSales
	where Id = @id

	select scope_identity()
GO
