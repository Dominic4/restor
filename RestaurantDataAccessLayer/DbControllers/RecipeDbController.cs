﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using RestaurantDataAccessLayer.Entities;

namespace RestaurantDataAccessLayer.DbControllers
{
    public class RecipeDbController
    {
        public static string ConnectionString = "Server = tcp:studyserver.database.windows.net,1433;Initial Catalog = RestaurantDB; Persist Security Info=False;User ID =MainUser; Password=ghbvf2GHBVF; MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout = 30";

        /// <summary>
        /// Add new recipe into table tbRecipes
        /// </summary>
        /// <param name="nameDish">name of dish</param>
        /// <param name="recipe">recipe of dish</param>
        /// <returns>return count of added recipes(1 or 0)</returns>
        public static bool AddRecipe(string nameDish, string recipe)
        {
            string sqlExpression = "AddRecipeSP";
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand(sqlExpression, connection);
                command.CommandType = CommandType.StoredProcedure;


                command.Parameters.Add(new SqlParameter {ParameterName = "@nameDish", Value = nameDish});
                command.Parameters.Add(new SqlParameter {ParameterName = "@recipe", Value = recipe});

                var result =  command.ExecuteScalar();
                return true;
            }
        }

        /// <summary>
        /// get list of all recipes from table tbRecipes
        /// </summary>
        /// <returns>list of recipes</returns>
        public static List<Recipe> GetRecipes()
        {
            string sqlExpression = "SelectRecipesSP";
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand(sqlExpression, connection);
                command.CommandType = CommandType.StoredProcedure;
                var reader = command.ExecuteReader();
                List<Recipe> list = new List<Recipe>();
                

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        int id = reader.GetInt32(0);
                        string nameDish = reader.GetString(1);
                        string resipe = reader.GetString(2);

                        list.Add(new Recipe(){Id = id, NameDish = nameDish, Recipy = resipe});
                    }
                }
                reader.Close();
                return list;
            }
        }

        /// <summary>
        /// Delete row with current ID
        /// </summary>
        /// <param name="id">ID</param>
        /// <returns>Return true if deleting success</returns>
        public static bool DeleteRecipe(int id)
        {
            string sqlExpression = "DeleteRecipySP";
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@id", Value = id });
                command.ExecuteNonQuery();
                return true;

            }
        }
    }
}
