﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestaurantDataAccessLayer.Entities;

namespace RestaurantDataAccessLayer.DbControllers
{
    public class SaleDbController
    {
        public static string ConnectionString = "Server = tcp:studyserver.database.windows.net,1433;Initial Catalog = RestaurantDB; Persist Security Info=False;User ID =MainUser; Password=ghbvf2GHBVF; MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout = 30";

        /// <summary>
        /// Add new sale into table tbSales
        /// </summary>
        /// <param name="idDish">id dish from table tbDishes</param>
        /// <param name="numOfSales">quantity of sales current dish</param>
        /// <param name="totalAmount">total summ from sale curr dishes</param>
        /// <returns>return true if sale added</returns>
        public static bool AddSale(int idDish, int numOfSales, double totalAmount)
        {
            string sqlExpression = "AddSaleSP";
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand(sqlExpression, connection);
                command.CommandType = CommandType.StoredProcedure;


                command.Parameters.Add(new SqlParameter { ParameterName = "@idDish", Value = idDish });
                command.Parameters.Add(new SqlParameter { ParameterName = "@numOfSales", Value = numOfSales });
                command.Parameters.Add(new SqlParameter { ParameterName = "@totalAmount", Value = totalAmount });
                var result = command.ExecuteScalar();
                return true;
            }
        }

        /// <summary>
        /// Select Sales from table tbSales
        /// </summary>
        /// <returns></returns>
        public static List<Sale> GetSales()
        {
            string sqlExpression = "SelectSalesSP";
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                command.CommandType = CommandType.StoredProcedure;
                List<Sale> list = new List<Sale>();
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int id = reader.GetInt32(0);
                        int idDish = reader.GetInt32(1);
                        int numOfSales = reader.GetInt32(2);
                        double totalAmount = reader.GetDouble(3);
                        list.Add(new Sale{Id = id,IdDish = idDish,NumOfSales = numOfSales,TotalAmount = totalAmount});
                    }
                }
                return list;
            }
        }
        /// <summary>
        /// Delete row with specified Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Return true if row was deleted</returns>
        public static bool DeleteSale(int id)
        {
            string sqlExpression = "DeleteSaleSP";
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@id", Value = id });
                command.ExecuteNonQuery();
                return true;

            }
        }

    }
}
