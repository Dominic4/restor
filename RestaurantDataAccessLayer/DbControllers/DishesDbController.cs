﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using RestaurantDataAccessLayer.Entities;

namespace RestaurantDataAccessLayer.DbControllers
{
    public class DishesDbController
    {
        public static string ConnectionString = "Server = tcp:studyserver.database.windows.net,1433;Initial Catalog = RestaurantDB; Persist Security Info=False;User ID =MainUser; Password=ghbvf2GHBVF; MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout = 30";

        /// <summary>
        /// Add new dishes into table tbDishes
        /// </summary>
        /// <param name="name">Name of Dish</param>
        /// <param name="recipeId">id recipe to current Dish</param>
        /// <param name="portionSize">size in unit of one portion</param>
        /// <param name="unit">unit measurement</param>
        /// <param name="price">price of one unit</param>
        /// <returns></returns>
        public static bool AddDish(string name, int recipeId, int portionSize, string unit, double price)
        {
            string sqlExpression = "AddDishSP";
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand(sqlExpression, connection);
                command.CommandType = CommandType.StoredProcedure;


                command.Parameters.Add(new SqlParameter { ParameterName = "@name", Value = name });
                command.Parameters.Add(new SqlParameter { ParameterName = "@recipeId", Value = recipeId });
                command.Parameters.Add(new SqlParameter { ParameterName = "@portionSize", Value = portionSize });
                command.Parameters.Add(new SqlParameter { ParameterName = "@unit", Value = unit });
                command.Parameters.Add(new SqlParameter { ParameterName = "@price", Value = price });

                var result = command.ExecuteScalar();
                return true;
            }
        }

        /// <summary>
        /// get list of all Dishes in table tbDishes
        /// </summary>
        /// <returns></returns>
        public static List<Dish> GetDishes()
        {
            string sqlExpression = "SelectDishesSP";
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand(sqlExpression, connection);
                command.CommandType = CommandType.StoredProcedure;
                var reader = command.ExecuteReader();
                List<Dish> list = new List<Dish>();
                

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        int id = reader.GetInt32(0);
                        string name = reader.GetString(1);
                        int recipeId = reader.GetInt32(2);
                        int portionSize = reader.GetInt32(3);
                        string unit = reader.GetString(4);
                        double price = reader.GetDouble(5);
                        list.Add(new Dish(){Id = id, Name = name, RecipeId = recipeId, PortionSize = portionSize, Unit = unit, Price = price});
                    }
                }
                reader.Close();
                return list;
            }
        }

        /// <summary>
        /// Delete row with current ID
        /// </summary>
        /// <param name="id">ID</param>
        /// <returns>Return true if deleting success</returns>
        public static bool DeleteDish(int id)
        {
            string sqlExpression = "DeleteDishSP";
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter { ParameterName = "@id", Value = id });
                command.ExecuteNonQuery();
                return true;

            }
        }
    }
}
